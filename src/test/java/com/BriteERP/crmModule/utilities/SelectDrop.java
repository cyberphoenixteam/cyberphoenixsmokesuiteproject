package com.BriteERP.crmModule.utilities;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SelectDrop {

    private static Select select;

    public static Select getSelect(WebElement element){
        if(select == null){
            select = new Select(element);
        }
        return select;
    }
}
