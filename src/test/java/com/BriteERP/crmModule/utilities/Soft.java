package com.BriteERP.crmModule.utilities;

import org.testng.asserts.SoftAssert;

public class Soft {
    private static SoftAssert softAssert;

    public static SoftAssert getSoftAssert(){
        if(softAssert == null){
            softAssert = new SoftAssert();
        }
        return softAssert;
    }
}
