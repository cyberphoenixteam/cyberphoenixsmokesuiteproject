package com.BriteERP.crmModule.test;

import com.BriteERP.crmModule.pages.HomePage;
import com.BriteERP.crmModule.pages.LoginPage;
import com.BriteERP.crmModule.pages.PipelinePage;

import com.BriteERP.crmModule.utilities.*;
import com.github.javafaker.Faker;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import com.BriteERP.crmModule.pages.QuotationsPage;
import com.BriteERP.crmModule.utilities.*;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

import java.util.Set;


public class Tests {

    /*
    Dinara
    TC: Validating importing excell and csv files
     */
    @Test
    public void validatingImportingCSVandExcellFiles() {

        LoginPage crmLP = new LoginPage();

        crmLP.loginToTheWebAppAsManager();

        HomePage hp = new HomePage();
        hp.goToCRMmodule();

        WebElement importButton = Driver.getDriver().findElement(By.xpath("//button[contains(text(),'Import')]"));
        importButton.click();

        WebElement fileInputBox = Driver.getDriver().findElement(By.xpath("//input[@class='oe_import_file_show form-control']"));
        fileInputBox.sendKeys("/Test Case.xlsx"+ Keys.ENTER);

        String attributeValue = fileInputBox.getAttribute("placeholder");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertFalse(attributeValue.contains("No file chosen"), "Verification of importing excell file FAILED!");

        softAssert.assertAll();
    }
    /*
    Gabriel
    TC: Validating calendar presence
     */


    @Test
    public void valiadatingCalendarPresence(){

        LoginPage briteLogin = new LoginPage();
        briteLogin.loginToTheWebAppAsUser();
        HomePage homePage = new HomePage();
        homePage.goToCRMmodule();

        PipelinePage pipelinePage = new PipelinePage();

       Soft.getSoftAssert().assertTrue(pipelinePage.calendarButton.isDisplayed(),"Calendar Button is not displayed");
       pipelinePage.calendarButton.click();
       Soft.getSoftAssert().assertTrue(pipelinePage.calendarTodayButton.isDisplayed(),"Calendar button is not clickable");

       Soft.getSoftAssert().assertAll();

    }

    /*
      Orhun
      TC: Validating activity types
      */

    @Test

    public void validatingCreatingNewActivityTypes () {

        LoginPage crmLP = new LoginPage();
        crmLP.loginToTheWebAppAsManager();

        HomePage hp = new HomePage();
        hp.goToCRMmodule();

        //CRM Module Click
        WebElement CRMModuleButton = Driver.getDriver().findElement(By.xpath("//a[@href='/web#menu_id=261&action=365']"));
        CRMModuleButton.click();

        //Activity Types Click
        WebElement activityTypeButton = Driver.getDriver().findElement(By.linkText("Activity Types"));
        activityTypeButton.click();

        //Create Button click
        WebDriverWait wait = new WebDriverWait(Driver.getDriver(), 5);
        WebElement createButton = Driver.getDriver().findElement(By.xpath("//button[@class='btn btn-primary btn-sm o_list_button_add']"));
        wait.until(ExpectedConditions.elementToBeClickable(createButton));
        createButton.click();

        //Creating the activity type by using faker
        WebElement inputName = Driver.getDriver().findElement(By.xpath("//input[@class='o_field_char o_field_widget o_input o_required_modifier']"));
        Faker faker = new Faker();
        String nameSendKeys = faker.name().name();
        inputName.sendKeys(nameSendKeys);
        WebElement categorySelect = Driver.getDriver().findElement(By.xpath("//select[@class='o_input o_field_widget']"));
        String optionWillbeSelected = "Meeting";
        SelectDrop.getSelect(categorySelect).selectByVisibleText(optionWillbeSelected);
        WebElement summaryBox = Driver.getDriver().findElement(By.xpath("//input[@class='o_field_char o_field_widget o_input']"));
        String summarySendKeys = faker.app().author();
        summaryBox.sendKeys(summarySendKeys);
        WebElement numberOfDays = Driver.getDriver().findElement(By.xpath("//input[@class='o_field_integer o_field_number o_field_widget o_input']"));
        String numberOfDaysSendKeys = faker.number().digit();
        numberOfDays.sendKeys(numberOfDaysSendKeys);
        WebElement saveButton = Driver.getDriver().findElement(By.xpath("//button[@class='btn btn-primary btn-sm o_form_button_save']"));
        saveButton.click();

        //Verifications
        String verifyName = Driver.getDriver().findElement(By.xpath("//span[@class='o_field_char o_field_widget o_required_modifier']")).getText();
        String verifyCategory = Driver.getDriver().findElement(By.xpath("//span[.='Meeting']")).getText();
        String verifySummary = Driver.getDriver().findElement(By.xpath("//span[@class='o_field_char o_field_widget']")).getText();
        String verifyNumberOfDays = Driver.getDriver().findElement(By.xpath("//span[@class='o_field_integer o_field_number o_field_widget']")).getText();
        Assert.assertEquals(nameSendKeys , verifyName , "Name of activity type failed");
        Assert.assertEquals(optionWillbeSelected , verifyCategory, "Category of activity type failed");
        Assert.assertEquals(summarySendKeys , verifySummary , "Summary of activity type failed");
        Assert.assertEquals(numberOfDaysSendKeys , verifyNumberOfDays , "Number of days of activity type failed");

    }
    /*
      Orhun
        TC: Validating deleting an activity type
      */

    @Test

    public void validatingDeleteAnActivityType () {


        String verifyNameBeforeDeletion = Driver.getDriver().findElement(By.xpath("//span[@class='o_field_char o_field_widget o_required_modifier']")).getText();
        System.out.println(verifyNameBeforeDeletion);
        WebElement actionButton = Driver.getDriver().findElement(By.xpath("(//span[@class='caret'])[3]"));
        actionButton.click();
        WebElement deleteButton = Driver.getDriver().findElement(By.xpath("//a[@data-index='0']"));
        deleteButton.click();
        WebElement okButton = Driver.getDriver().findElement(By.xpath("//span[.='Ok']"));
        okButton.click();
        WebElement activiTypeButton = Driver.getDriver().findElement(By.xpath("//a[.='Activity Types']"));
        Action.getActions().moveToElement(activiTypeButton).click().perform();
        List<WebElement> allNames = Driver.getDriver().findElements(By.xpath("//tbody/tr/td[3]"));

        for (WebElement w : allNames) {

            Assert.assertTrue(!verifyNameBeforeDeletion.equals(w.getText()) , "Deletion of activity type failed");

        }

    }

    /*
   Raikhan
   TC: Create an Opportunity in Pipeline field
    */

    @Test (priority = 1)
    public void verifyCreatingOpportunity(){

        LoginPage crmLP = new LoginPage();
        crmLP.loginToTheWebAppAsManager();

        HomePage hp = new HomePage();
        hp.goToCRMmodule();


        Driver.getDriver().findElement(By.xpath("//button[@class='btn btn-primary btn-sm o-kanban-button-new']")).click();
        Driver.getDriver().findElement(By.xpath("//td[@class='o_td_label']/label[@class='o_form_label o_required_modifier']")).isDisplayed();

        String actualText1 = Driver.getDriver().findElement(By.xpath("(//td[@class='o_td_label'])[1]")).getText();
        String expectedText1 = "Opportunity Title";

        Soft.getSoftAssert().assertEquals(actualText1,expectedText1,"Actual text doesnt match to expected Opportunity Title");

        Faker faker = new Faker();
        System.out.println(faker.name().name());
        Driver.getDriver().findElement(By.name("name")).sendKeys(faker.name().name());

        String actualText2 = Driver.getDriver().findElement(By.xpath("(//td[@class='o_td_label'])[2]")).getText();
        String expectedText2 = "Customer";
        Soft.getSoftAssert().assertEquals(actualText2,expectedText2,"Actual text doesnt match to expected Customer");


        Driver.getDriver().findElement(By.xpath("(//input[contains(@class,'autocomplete')])[1]")).click();
        Driver.getDriver().findElement(By.xpath("//li[@class='ui-menu-item']")).click();

        String actualText3 = Driver.getDriver().findElement(By.xpath("(//td[@class='o_td_label'])[3]")).getText();
        String expectedText3 = "Expected Revenue";
        Soft.getSoftAssert().assertEquals(actualText3,expectedText3,"Actual text doesnt match to expected Expected Revenue");



        Driver.getDriver().findElement(By.xpath("//input[@class='o_field_float o_field_number o_field_widget o_input']")).sendKeys("452");

        String actualText4 = Driver.getDriver().findElement(By.xpath("(//td[@class='o_td_label'])[4]")).getText();
        String expectedText4 = "Priority";
        Soft.getSoftAssert().assertEquals(actualText4,expectedText4,"Priority");


        Driver.getDriver().findElement(By.xpath("//td//div[@class='o_priority o_field_widget']//a[@title='High']")).click();

        Driver.getDriver().findElement(By.xpath("//span[contains(text(),'Create')]")).click();


//


    }

    /*
    Dinara
    TC: Validating creating a new Sales Channel
     */

    @Test
    public void validationOfCreatingSalesChannel() throws InterruptedException{

        LoginPage crmLP = new LoginPage();
        crmLP.loginToTheWebAppAsManager();

        //locating and clicking the CRM module menu item
        HomePage hp = new HomePage();
        hp.goToCRMmodule();

        //locating and clicking to Sales Channel functionality under Configuration
        WebElement salesChannel = Driver.getDriver().findElement(By.xpath("//a[@data-menu='267']"));
        Thread.sleep(3000);
        salesChannel.click();


        Thread.sleep(3000);
        //locating and clicking Create button for Sales Channels
        WebElement createButton = Driver.getDriver().findElement(By.xpath("//div[@class='o_cp_left']//button"));
        createButton.click();

        //locating to elements of the form sheet
        WebElement salesteamNameInputBox = Driver.getDriver().findElement(By.xpath("//input[@class='o_field_char o_field_widget o_input o_required_modifier']"));
        WebElement quotations = Driver.getDriver().findElement(By.xpath("//label[.='Quotations']"));
        WebElement pipelineLabel = Driver.getDriver().findElement(By.xpath("//label[.='Pipeline']"));
        WebElement channelLeaderInputbox = Driver.getDriver().findElement(By.xpath("//div[@name='user_id']//input"));
        WebElement emailInputbox = Driver.getDriver().findElement(By.xpath("//div[@name='edit_alias']//input"));
        WebElement addButton = Driver.getDriver().findElement(By.xpath("//button[@class='btn btn-primary btn-sm o-kanban-button-new']"));

        //Sending the keys for reqiured input boxes
        channelLeaderInputbox.sendKeys("Administrator"+Keys.ENTER);
        Faker faker = new Faker();
        emailInputbox.sendKeys(faker.internet().emailAddress());
        addButton.click();

        //Verifying the elements
        Soft.getSoftAssert().assertTrue(salesteamNameInputBox.isDisplayed(),
                "Verification of the default value of Sales Channels name input box FAILED!");
        Soft.getSoftAssert().assertTrue(quotations.isDisplayed(),"The label Quotaions is not Displayed by default. Verification FAILED!");
        Soft.getSoftAssert().assertTrue(pipelineLabel.isDisplayed(),"The label Pipeline is not Displayed");

        //locating to the list of sales members for adding him/her for New Sales Channel as a member
        WebElement channelsMemberAltynaiTurgaeva = Driver.getDriver().findElement(By.xpath("//table[starts-with(@class,'o_list_view')]/tbody//tr[3]/td[1]"));
        channelsMemberAltynaiTurgaeva.click();

        //locating and clicking create button after adding the member
        WebElement createButton2 = Driver.getDriver().findElement(By.xpath("//button[@class='btn btn-sm btn-primary']"));
        createButton2.click();

        //locating and clicking the Save & Close button
        WebElement saveAndClose = Driver.getDriver().findElement(By.xpath("//span[.='Save  & Close']"));
        saveAndClose.click();

        //Verifying if the error message is displayed or not
        WebElement errorMessage = Driver.getDriver().findElement(By.xpath("//div[@class='o_notification undefined o_error']"));
        Soft.getSoftAssert().assertFalse(errorMessage.isDisplayed(),"Verification of creating the Sales Channels FAILED! " +
                "Error message is displayed");
        Soft.getSoftAssert().assertAll();

    }

    /*
    Marianna
    TC:Negative scenario. As a manager I shouldn't be able to type any letters in the phone field
     */

    @Test
    public void checkinggPhoneFieldInsideTheSalesChannel() throws  InterruptedException{

        LoginPage login = new LoginPage();
        login.loginToTheWebAppAsManager();

        HomePage hPage = new HomePage();
        hPage.goToCRMmodule();

        WebElement SalesChannelField = Driver.getDriver().findElement(By.xpath("//a[@href = '/web#menu_id=267&action=242']"));
        SalesChannelField.click();

        WebElement EuropeSalesChannel = Driver.getDriver().findElement(By.xpath("//table[@class = 'o_list_view table table-condensed table-striped o_list_view_ungrouped']//tbody//tr[2]/td[2]"));
        EuropeSalesChannel.click();

        WebElement AddFolowerIcon = Driver.getDriver().findElement(By.xpath("//div[@class = 'o_followers_title_box']//button[@class='btn btn-sm btn-link dropdown-toggle']"));
        AddFolowerIcon.click();

        WebElement AddFollower = Driver.getDriver().findElement(By.linkText("Add Followers"));
        AddFollower.click();

        WebElement dropDownAddContacts = Driver.getDriver().findElement(By.xpath("//div[@class = 'o_field_many2manytags o_input o_field_widget']//div[@class='o_input_dropdown']"));
        dropDownAddContacts.click();

        Thread.sleep(1000);

        WebElement Create = Driver.getDriver().findElement(By.xpath("//li[@class = 'o_m2o_dropdown_option ui-menu-item'][2]")) ;
        Create.click();


        Soft.getSoftAssert().assertAll();

        Faker faker = new Faker();
        WebElement NameInput = Driver.getDriver().findElement(By.xpath("//div[@class = 'oe_title']//input[@name = 'name']"));
        NameInput.sendKeys(faker.company().name());

        WebElement jobPosition = Driver.getDriver().findElement(By.xpath("//table[@class = 'o_group o_inner_group']//tbody//tr//td//input[@class ='o_field_char o_field_widget o_input']"));
        jobPosition.sendKeys(faker.job().position());

        WebElement emailBox = Driver.getDriver().findElement(By.xpath("//table[@class = 'o_group o_inner_group']//tbody//tr//td//input[@class ='o_field_email o_field_widget o_input o_required_modifier']"));
        emailBox.sendKeys(faker.internet().emailAddress());

        WebElement phoneField = Driver.getDriver().findElement(By.xpath("//table[@class = 'o_group o_inner_group']//tbody//tr//td//input[@name = 'phone']"));
        phoneField.sendKeys(faker.phoneNumber().cellPhone());
        phoneField.sendKeys(faker.funnyName().name());

        WebElement saveButton = Driver.getDriver().findElement(By.xpath("//span [.='Save']"));
        saveButton.click();

        WebElement senEmailCheckBox = Driver.getDriver().findElement(By.xpath("//div[@name ='send_mail']"));

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(senEmailCheckBox.isEnabled(),"Failed");

        WebElement textOfEmail = Driver.getDriver().findElement(By.xpath("//div[@class = 'note-editing-area']//div[@class = 'note-editable panel-body']"));
        String actualText = textOfEmail.getText();
        String expectedText = "Hello,\n" + "EventsCRM_Manager7 invited you to follow Sales Channel document: Europe";
        softAssert.assertEquals(actualText,expectedText);
        Thread.sleep(1000);

        WebElement addFollowerButton = Driver.getDriver().findElement(By.xpath("//div[@class ='modal-footer']//div//button//span[.='Add Followers']"));
        addFollowerButton.click();
        Soft.getSoftAssert().assertAll();
    }

    /*
    Marianna
    TC:Positive Scenario. As a manager I should be able to add new Recipient on "Add Follower" field
     */
    @Test
    public void AddFollower() throws InterruptedException {

        LoginPage login = new LoginPage();
        login.loginToTheWebAppAsManager();

        HomePage hPage = new HomePage();
        hPage.goToCRMmodule();

        WebElement SalesChannelField = Driver.getDriver().findElement(By.xpath("//a[@href = '/web#menu_id=267&action=242']"));
        SalesChannelField.click();

        WebElement EuropeSalesChannel = Driver.getDriver().findElement(By.xpath("//table[@class = 'o_list_view table table-condensed table-striped o_list_view_ungrouped']//tbody//tr[2]/td[2]"));
        EuropeSalesChannel.click();

        WebElement AddFollowerIcon = Driver.getDriver().findElement(By.xpath("//div[@class = 'o_followers_title_box']//button[@class='btn btn-sm btn-link dropdown-toggle']"));
        AddFollowerIcon.click();

        WebElement AddFollower = Driver.getDriver().findElement(By.linkText("Add Followers"));
        AddFollower.click();

        WebElement dropDownAddContacts = Driver.getDriver().findElement(By.xpath("//div[@class = 'o_field_many2manytags o_input o_field_widget']//div[@class='o_input_dropdown']"));
        dropDownAddContacts.click();

        Thread.sleep(1000);

        WebElement Create = Driver.getDriver().findElement(By.xpath("//li[@class = 'o_m2o_dropdown_option ui-menu-item'][2]")) ;
        Create.click();


        Faker faker = new Faker();
        WebElement NameInput = Driver.getDriver().findElement(By.xpath("//div[@class = 'oe_title']//input[@name = 'name']"));
        NameInput.sendKeys(faker.company().name());

        WebElement jobPosition = Driver.getDriver().findElement(By.xpath("//table[@class = 'o_group o_inner_group']//tbody//tr//td//input[@class ='o_field_char o_field_widget o_input']"));
        jobPosition.sendKeys(faker.job().position());

        WebElement emailBox = Driver.getDriver().findElement(By.xpath("//table[@class = 'o_group o_inner_group']//tbody//tr//td//input[@class ='o_field_email o_field_widget o_input o_required_modifier']"));
        emailBox.sendKeys(faker.internet().emailAddress());

        WebElement phoneField = Driver.getDriver().findElement(By.xpath("//table[@class = 'o_group o_inner_group']//tbody//tr//td//input[@name = 'phone']"));
        phoneField.sendKeys(faker.phoneNumber().cellPhone());

        WebElement saveButton = Driver.getDriver().findElement(By.xpath("//span [.='Save']"));
        saveButton.click();

        WebElement senEmailCheckBox = Driver.getDriver().findElement(By.xpath("//div[@name ='send_mail']"));
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(senEmailCheckBox.isEnabled(),"Failed");

        WebElement textOfEmail = Driver.getDriver().findElement(By.xpath("//div[@class = 'note-editing-area']//div[@class = 'note-editable panel-body']"));
        String actualText = textOfEmail.getText();
        String expectedText = "Hello,\n" + "EventsCRM_Manager7 invited you to follow Sales Channel document: Europe";
        softAssert.assertEquals(actualText,expectedText);
        Thread.sleep(1000);

        WebElement addFollowerButton = Driver.getDriver().findElement(By.xpath("//div[@class ='modal-footer']//div//button//span[.='Add Followers']"));
        addFollowerButton.click();
        Soft.getSoftAssert().assertAll();
    }


 /*
 Aiperi
    TC:Negative Scenario. As a user I should not be able to create new Quotations/Invoice CP-107
  */

  @Test
    public void UserCreationNewQuotation(){

    LoginPage login = new LoginPage();
    login.loginToTheWebAppAsUser();
    HomePage hp = new HomePage();
    hp.goToCRMmodule();

   WebElement quotationButton = Driver.getDriver().findElement(By.xpath("//a[@data-menu-xmlid=\"sale_crm.sale_order_menu_quotations_crm\"]"));
   quotationButton.click();

   Driver.getDriver().findElement(By.xpath("//button[@class=\"btn btn-primary btn-sm o_list_button_add\"]")).click();

    String labelText = "Quotations";
    WebElement labelTextDisplayed =  Driver.getDriver().findElement(By.xpath("//div[@class=\"o_control_panel o_breadcrumb_full\"]"));

    Assert.assertTrue(labelTextDisplayed.getText().contains(labelText),"Expected text does not dispalyed");

    ExplicitWait.getWait();

    WebElement addAnItemButton =  Driver.getDriver().findElement(By.linkText("Add an item"));
    addAnItemButton.click();

    WebElement product =  Driver.getDriver().findElement(By.xpath("(//input[@class='o_input ui-autocomplete-input'])[17]"));

    product.sendKeys("CAP");

    ExplicitWait.getWait();

    product.click();
    Driver.getDriver().findElement(By.xpath("//button[@class='btn btn-sm btn-primary'][1]")).click();

    ExplicitWait.getWait();

    Driver.getDriver().findElement(By.xpath("//span[.='Create']")).click();

    Driver.getDriver().findElement(By.xpath("//span[.='Save']")).click();

    WebElement errorHeader =  Driver.getDriver().findElement(By.xpath("(//h4[@class='modal-title'])[1]"));
    String errorSentenceHeader = "Create Order Lines";

    Assert.assertTrue(errorHeader.getText().equals(errorSentenceHeader),"The error text Not Displayed");

}


  /*
   Aiperi
    TC:Positive Scenario. As a user I should be able to enter the existing company name, system should show ONLY entered company Quotations CP-149
     */

    @Test
    public void userAbleToSortTableBySelectedCompany()throws InterruptedException{


    LoginPage login = new LoginPage();
    login.loginToTheWebAppAsUser();
    HomePage hp = new HomePage();
    hp.goToCRMmodule();

    Thread.sleep(3000);

    WebElement quotationButton = Driver.getDriver().findElement(By.xpath("(//span[@class='oe_menu_text'])[18]"));
    quotationButton.click();

    Thread.sleep(3000);


    WebElement headTextActual = Driver.getDriver().findElement(By.xpath("//ol[@class='breadcrumb']//li"));
    String headText = "Quotations";

    ExplicitWait.getWait();

    Assert.assertTrue(headTextActual.getText().equals(headText),"The head text not matching");

    ExplicitWait.getWait();

    WebElement searchButton =Driver.getDriver().findElement(By.cssSelector(".o_searchview_input"));
    searchButton.sendKeys("Al Shaya"+Keys.ENTER);

}


 /*
    Altynai
    TC :quotationsImport
     */


    @Test
    public void quotationsImportButtonAltynai() throws InterruptedException {

        LoginPage login = new LoginPage();
        login.loginToTheWebAppAsManager();

        HomePage hp = new HomePage();
        hp.goToCRMmodule();
        WebElement quatation  = Driver.getDriver().findElement(By.xpath("//span[contains(text(),'Quotations')][1]"));
        quatation.click();
        WebElement imp = Driver.getDriver().findElement(By.xpath("//button[@class='btn btn-sm btn-default o_button_import']"));
        imp.click();
        WebElement fileInput = Driver.getDriver().findElement(By.xpath("//input[@class='oe_import_file_show form-control']"));
        //   fileInput.sendKeys("/Quatation_test.xlsx");
        String actualURL = Driver.getDriver().getCurrentUrl();
        String keyWordInUrl = "import";

        Assert.assertTrue(actualURL.contains(keyWordInUrl),"FAILED, expected URL is not matching with Actual url");

        Soft.getSoftAssert().assertAll();

    }

    /*Eldiar
    validationForMyOrdersQuotationsPage
   */
    @Test
    public void validationForMyOrdersQuotationsPage() throws InterruptedException{
        LoginPage briteLogin = new LoginPage();
        briteLogin.loginToTheWebAppAsUser();
        HomePage homePage = new HomePage();
        homePage.goToCRMmodule();

        QuotationsPage QP = new QuotationsPage();
        Soft.getSoftAssert().assertTrue(QP.QuotationsButton.isDisplayed(),"QuotationsButton is not Diplayed");
        Thread.sleep(2000);
        QP.QuotationsButton.click();
        Thread.sleep(2000);
        Soft.getSoftAssert().assertTrue(QP.plusMinusButton.isDisplayed(),"plusMinusButton is not Dsplayed");
        QP.plusMinusButton.click();

        Soft.getSoftAssert().assertTrue(QP.filterButton.isDisplayed(),"FiltersButton is not Dsplayed");
        Thread.sleep(2000);
        QP.filterButton.click();
        Thread.sleep(2000);
        Soft.getSoftAssert().assertTrue(QP.myOrdersButton.isDisplayed(),"myOrdersButton is not Dsplayed");
        QP.myOrdersButton.click();
        Soft.getSoftAssert().assertAll();

    }

}