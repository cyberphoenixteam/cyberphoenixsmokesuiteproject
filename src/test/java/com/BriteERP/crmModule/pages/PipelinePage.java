package com.BriteERP.crmModule.pages;

import com.BriteERP.crmModule.utilities.Driver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PipelinePage {

    public WebDriver driver;

    public PipelinePage(){
        driver = Driver.getDriver();
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//button[@aria-label='calendar']")
    public WebElement calendarButton;

    @FindBy(xpath = "//button[@class='o_calendar_button_today btn btn-sm btn-primary']")
    public WebElement calendarTodayButton;


    @FindBy(xpath = ("//button[@class='btn btn-primary btn-sm o-kanban-button-new']"))
    public WebElement createButton;

}
