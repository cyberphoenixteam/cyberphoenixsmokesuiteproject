package com.BriteERP.crmModule.pages;

import com.BriteERP.crmModule.utilities.Driver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class Activities {

    public WebDriver driver;

    public Activities(){
        driver = Driver.getDriver();
        PageFactory.initElements(driver,this);
    }
}
