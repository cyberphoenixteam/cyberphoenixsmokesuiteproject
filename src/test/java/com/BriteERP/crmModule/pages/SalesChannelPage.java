package com.BriteERP.crmModule.pages;

import com.BriteERP.crmModule.utilities.Driver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class SalesChannelPage {
    public WebDriver driver;

    public SalesChannelPage(){
        driver = Driver.getDriver();
        PageFactory.initElements(driver,this);
    }
}
